pipeline {
    agent any

    parameters {
        string(name: 'NEXUS_VM_IP', defaultValue: 'default-nexus-vm-ip', description: 'IP address of the Nexus VM')
        string(name: 'NODEJS_VM_IP', defaultValue: 'default-nodejs-vm-ip', description: 'IP address of the Node.js VM')
    }

    environment {
        NEXUS_VM_IP = "${params.NEXUS_VM_IP}"
        NODEJS_VM_IP = "${params.NODEJS_VM_IP}"
        DOCKER_IMAGE = "${NEXUS_VM_IP}:8083/repository/docker-hosted/my-nodejs-app"
        GITLAB_CREDENTIALS_ID = 'gitlab-creds'
        NEXUS_CREDENTIALS_ID = 'nexus-creds'
        VERSION = '1.0'
        BUILD_NUMBER = '1'
    }

    stages {
        stage('Checkout Code') {
            steps {
                git branch: 'main', credentialsId: "${GITLAB_CREDENTIALS_ID}", url: 'https://gitlab.com/ArmaKeoN/DevStream360.git'
            }
        }

        stage('Prepare Application Code') {
            steps {
                script {
                    sh """
                    sed -i 's|<nodejs_vm_ip>|${NODEJS_VM_IP}|g' ./nodejs/app/index.html
                    """
                }
            }
        }

        stage('Build and Push Docker Image') {
            steps {
                script {
                    try {
                        sh 'docker build -t ${DOCKER_IMAGE}:${VERSION} ./nodejs'
                        withCredentials([usernamePassword(credentialsId: "${NEXUS_CREDENTIALS_ID}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                            sh "echo $PASSWORD | docker login ${NEXUS_VM_IP}:8083 --username $USERNAME --password-stdin"
                            sh 'docker push ${DOCKER_IMAGE}:${VERSION}'
                        }
                    } catch (Exception e) {
                        echo "Failed to build or push Docker image: ${e}"
                        throw new RuntimeException('Build or push failed')
                    }
                }
            }
        }

        stage('Prepare Docker Compose File') {
            steps {
                script {
                    sh """
                    sed -i 's|<nexus_vm_ip>:8083/repository/docker-hosted/my-nodejs-app:1.0|${DOCKER_IMAGE}:${VERSION}|' ./nodejs/docker-compose.yaml
                    """
                }
            }
        }

        stage('Deploy to VM') {
            steps {
                sshagent(credentials: ['vm-ssh-creds']) {
                    withCredentials([usernamePassword(credentialsId: 'nexus-creds', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh """
                        # Setup SSH known hosts to avoid interactive prompt
                        mkdir -p /var/jenkins_home/.ssh
                        chmod 700 /var/jenkins_home/.ssh
                        if ! grep -q ${NODEJS_VM_IP} /var/jenkins_home/.ssh/known_hosts; then
                            ssh-keyscan ${NODEJS_VM_IP} >> /var/jenkins_home/.ssh/known_hosts
                        fi
                        chmod 600 /var/jenkins_home/.ssh/known_hosts

                        # Copy docker-compose.yaml to the remote server
                        scp -o BatchMode=yes ./nodejs/docker-compose.yaml root@${NODEJS_VM_IP}:/srv/nodejs-app/

                        # Execute deployment commands on the remote server
                        ssh -o BatchMode=yes root@${NODEJS_VM_IP} '
                            set -e  # Ensure the script exits on first error

                            # Docker registry login
                            echo "$PASSWORD" | docker login ${NEXUS_VM_IP}:8083 --username "$USERNAME" --password-stdin

                            # Navigate to the app directory and manage containers with Docker Compose
                            cd /srv/nodejs-app/
                            docker-compose down  # Stop and remove current containers if exist
                            docker-compose pull  # Ensure latest images are pulled
                            docker-compose up -d  # Start the containers in the background
                        '
                        """
                    }
                }
            }
        }
    }
}
