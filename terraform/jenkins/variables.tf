variable "do_token" {
  description = "DigitalOcean API token"
  type        = string
  sensitive   = true
}

variable "project_name" {
  description = "DevStream360"
  type        = string
  default     = "DevStream360"  # Change this to your desired project name
}