# DevStream360

## Overview

DevStream360 is a project designed to optimize a DevOps learning environment and infrastructure management costs through automation and cloud-based technologies. The primary objective of this project is to create a cost-effective, automated environment for individuals learning DevOps principles, particularly through courses such as 'Tech with Nana'.

![Alt text](<Flow Diagram.png>)

## Tech Stack

- **Terraform Cloud:** Utilized for infrastructure as code (IaC) to provision and manage cloud resources in a secure, efficient manner. It also handles secrets and state files management.
- **Ansible:** Integrated within a GitLab CI/CD pipeline to configure the provisioned resources, ensuring consistent and repeatable setups.
- **GitLab:** Serves as a central repository for code storage and as a platform for the continuous integration and deployment pipeline. It hosts the GitLab Runner in a Docker container to facilitate these processes.
- **Jenkins:** Deployed via a Docker image on a provisioned Digital Ocean droplet. It is preconfigured with a CI/CD pipeline that builds and pushes Docker images to the Nexus repository.
- **Nexus:** Hosted on a Digital Ocean droplet, it provides a repository manager that supports various package formats and adds a layer of organization for build artifacts. Specifically, Nexus hosts Docker images built and pushed via the Jenkins pipeline.
- **Digital Ocean:** The cloud provider where resources (droplets) are provisioned to host the learning environments. Separate droplets are also set up to host Docker Compose applications, including a Node.js web app, Mongo Express, and MongoDB.
- **Bash & Python:** Scripting languages used for creating APIs and automation scripts that interact with Terraform Cloud and Digital Ocean APIs for provisioning and de-provisioning of resources.
- **Chat GPT:** Instrumental in helping organize, document, troubleshoot, and code the project. It serves as a support mechanism and facilitates learning through conversational AI.
- **Docker:** Utilized to containerize applications, including the Ansible environment, ensuring a consistent execution environment for configuration management. Docker is also used in various other capacities, such as hosting the GitLab Runner, Nexus, Jenkins, and the Node.js web app via Docker Compose.

## Purpose and Workflow

- **Cost-Effectiveness:** The inception of DevStream360 stemmed from the need to reduce costs incurred from cloud resources. By automating the setup and teardown of learning environments, it minimizes the expenses associated with idle resources.
- **Automation:** The process begins with a GitLab CI/CD pipeline triggered with a simple click. It deploys a Docker container running Ansible, which then proceeds to configure a freshly provisioned Digital Ocean droplet.
- **On-Demand Provisioning:** A key feature of DevStream360 is the ability to provision and configure the Nexus repository manager and Jenkins on-demand within the droplet. This is facilitated through API scripts written in Bash and Python, which also communicate with Terraform Cloud's API.
- **Docker Integration:** Separate droplets are provisioned to host Docker Compose applications. These applications include a Node.js web app, Mongo Express, and MongoDB. Additionally, Docker is used to host the GitLab Runner, Jenkins, and Nexus, ensuring a consistent and isolated environment for all services.
- **Jenkins CI/CD Pipeline:** Jenkins is preconfigured with a CI/CD pipeline that builds Docker images from the source code, pushes them to the Nexus repository, and then runs Docker Compose on the Node.js web app server. This setup ensures a streamlined and automated deployment process.
- **Terraform Cloud Integration:** By leveraging Terraform Cloud, DevStream360 ensures that the provisioning process is seamless, with access tokens and sensitive information securely managed, and state files properly maintained.
- **Configurability and Repeatability:** The combination of Terraform and Ansible within a Docker image allows for consistent configuration and the ability to recreate the environment as needed without manual intervention.
- **Destruction and Re-provisioning:** The same infrastructure that is effortlessly provisioned can also be destroyed with equal ease, thanks to the API scripts. This ensures that when the learning environment is no longer needed, it can be taken down to avoid unnecessary costs.

In essence, DevStream360 provides a smart, automated, and cost-aware system for learning and practicing DevOps principles, allowing users to focus more on learning and less on the overhead of managing and maintaining their learning infrastructure.