terraform {
  backend "remote" {
    organization = "jonahlozano03"

    workspaces {
      name = "DigitalOcean_Nodejs_VM"
    }
  }
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.37.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

# Data source to retrieve the existing SSH key by name
data "digitalocean_ssh_key" "my_key" {
  name = "my-droplet-key" # Use the name of your SSH key as shown in DigitalOcean
}

# Create or reference a DigitalOcean project
data "digitalocean_project" "devstream360_project" {
  name = var.project_name
}

# Your resource definitions remain the same

resource "digitalocean_droplet" "nodejs-server" {
  image    = "ubuntu-20-04-x64"
  name     = "nodejs-server"
  region   = "ams3"
  size     = "s-2vcpu-4gb"
  ssh_keys = [data.digitalocean_ssh_key.my_key.id] # Referencing the SSH key ID
}

resource "digitalocean_firewall" "nodejs_server_access" {
  name = "nodejs-server-firewall"

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"] # All IPv4 and IPv6
  }

  # Rule for custom port 8081 from all IPv4 and IPv6
  inbound_rule {
    protocol         = "tcp"
    port_range       = "8081"
    source_addresses = ["0.0.0.0/0", "::/0"] # All IPv4 and IPv6
  }

  # Rule for custom port 3000 from all IPv4 and IPv6
  inbound_rule {
    protocol         = "tcp"
    port_range       = "3000"
    source_addresses = ["0.0.0.0/0", "::/0"] # All IPv4 and IPv6
  }

  # Allow all outbound ICMP traffic
  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # Allow all outbound TCP traffic
  outbound_rule {
    protocol              = "tcp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # Allow all outbound UDP traffic
  outbound_rule {
    protocol              = "udp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # Assuming you have a droplet you want to attach the firewall to
  # Replace with your droplet ID or use a dynamic reference to a droplet resource.
  droplet_ids = [digitalocean_droplet.nodejs-server.id]
}

# Assign the droplet to the project
resource "digitalocean_project_resources" "project_assignment" {
  project = data.digitalocean_project.devstream360_project.id
  resources = [
    "do:droplet:${digitalocean_droplet.nodejs-server.id}",
    "do:firewall:${digitalocean_firewall.nodejs_server_access.id}"
  ]
}

output "ip_address" {
  value = digitalocean_droplet.nodejs-server.ipv4_address
}
